#!/bin/sh

kill -15 $(cat lastpid.txt)
python main.py &
echo $! > lastpid.txt
